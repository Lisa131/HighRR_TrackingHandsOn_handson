#ifndef INCLUDE_TFRPARTICLE_H
#define INCLUDE_TFRPARTICLE_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of MC particles
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TEveTrack.h"
#include "TLorentzVector.h"

//custom libraries
#include "TFRMCHit.h"
#include "TFRCluster.h"

using namespace std;

class TFRParticle : public TEveRecTrackD {
 public:

  /*! Empty constructor */
  TFRParticle(){
    clusters = new TFRClusters();
    hits = new TFRMCHits();
    lorentz_vect = TLorentzVector(0,0,0,0);
    is_signal = false;
  };
  
  /*! Standard constructor */
  TFRParticle(TVector3 vertex, TVector3 momentum,
	      double mass, double charge);
  
  /*! Destructor */
  virtual ~TFRParticle( ){ };

  /*! Get the vertex */
  inline TVector3 GetVertex() const {return vertex;};

  /*! Get the beta factor */
  inline double GetBeta() const {return lorentz_vect.Beta();};
  
  /*! Get the momentum vector */
  inline TVector3 GetMomentum() const {return momentum;};

  /*! Get the momentum modulus */
  inline double GetMomentumMod() const {return momentum.Mag();}; 

  /*! Get tx */
  inline double GetTx() const {return fP[0]/fP[2];};
  
  /*! Get ty */
  inline double	GetTy() const {return fP[1]/fP[2];};
  
  /*! Get the charge */
  inline int GetCharge() const {return fSign;};

  /*! Get the q/p */
  inline double GetQoP() const {return fSign/momentum.Mag();};
  
  /*! Add the hit to the particle vector of hits */
  void AddHit(TFRMCHit *hit);

  /*! Add the cluster to the particle vector of clusters */
  void AddCluster(TFRCluster *cluster);
  
  /*! Returns the hits of the particle */
  inline TFRMCHits* GetHits(){return hits;};

  /*! Returns the particle clusters of the particle */
  inline TFRClusters* GetClusters(){return clusters;};

  /*! Set the particle ID */
  inline void SetID(int _ID){ID = _ID;};

  /*! Returns the particle ID */
  inline int GetID() const {return ID;};

  /*! Set the signal flag */
  inline void SetIsSignal(bool _is_signal){is_signal = _is_signal;};

  /*! Returns the signal flag */
  inline bool IsSignal() const {return is_signal;};

  /*! Returns the particle mass */
  inline double GetMass() const {return lorentz_vect.M();};
  
 protected:
  
 private:

  /*! Particle ID */
  int ID;

  /*! Vertex of the particle */
  TVector3 vertex;

  /*! Initial momentum of the particle */
  TVector3 momentum;
  
  /*! Lorentz vector representing the particle */
  TLorentzVector lorentz_vect;
  
  /*! Hits of the particle */
  TFRMCHits *hits;

  /*! Clusters of the particle */
  TFRClusters *clusters;

  /*! Flag for signal decaying particles */
  bool is_signal;
  
  //magic happens here
  ClassDef(TFRParticle, 1)
};

/*! Array of particles */
typedef TObjArray TFRParticles;

#endif // INCLUDE_TFRPARTICLE_H
