#ifndef INCLUDE_TFRMULTIPLESCATTERING_H
#define INCLUDE_TFRMULTIPLESCATTERING_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Library to handle the simulation of the multiple scattering
 *  For more informations: - PDG review on multiple scattering: http://pdg.lbl.gov/2016/reviews/rpp2016-rev-passage-particles-matter.pdf
 *                         - http://geant4.cern.ch/G4UsersDocuments/UsersGuides/PhysicsReferenceManual/html/node34.html
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TRandom3.h"

using namespace std;

class TFRMultipleScattering {

 public:

  /*! Empty constructor */
  TFRMultipleScattering(){
    random = new TRandom3();
    random->SetSeed(84);
  };
  
  /*! Destructor */
  ~TFRMultipleScattering( );

  /*! Returns the angular deviation with Gaussian approximation */
  inline double GetThetaGauss(const double x_over_Xzero,
			      const double beta, const double p, const int charge) const
      { return random->Gaus(0., GetThetaRMSGauss(x_over_Xzero, beta, p, charge)); };
  
  /*! Returns the rms theta for Gaussian approximation */
  inline double GetThetaRMSGauss(const double x_over_Xzero,
				 const double beta, const double p, const int charge) const
      { return (13.6/(beta*p))*abs(charge)*sqrt(x_over_Xzero)*(1. + 0.038*log(x_over_Xzero)); };
  
 protected:
  
 private:

  /*! (Pseudo-)random generator to simulate the scattering */
  TRandom3 *random;
  
};

#endif // INCLUDE_TFRMULTIPLESCATTERING_H
