#ifndef INCLUDE_TFRLAYER_H
#define INCLUDE_TFRLAYER_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a detector layer
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <iomanip>
#include <sstream>

//ROOT libraries
#include "TROOT.h"
#include "TEveTrack.h"
#include "TMatrixD.h"
#include "TVectorD.h"

using namespace std;

/*! Kind of supported sensors */
//enum TFRSensor {UNSET, PIXEL, STRIP};

class TFRLayer : public TObject {

 public:
  
  /*! Empty constructor */
  TFRLayer(){ };

  /*! Standard constructor */
  TFRLayer(const unsigned int _layer_id, const TEveVectorD _center_position,
           const double _phi, const double _theta, const double _psi,
           const double _x_size, const double _y_size, const double _dxdy,
           const std::string _sensor_type,
           const double _hit_resolution, const double _hit_efficiency,
           const double _noise_prob,
           const double _x_segm, const double _y_segm,
	   const double _x_over_Xzero);
  
  /*! Destructor */
  virtual ~TFRLayer( ){ };

  /*! Set the layer ID */
  inline void SetID(const unsigned int _layer_id) {layer_id = _layer_id;};
   
  /*! Get the layer ID */
  inline unsigned int GetID() const {return layer_id;};

  /*! Returns the center position */
  inline TEveVectorD GetPosition() const {return center_position;};
  
  /*! Get the versor ortogonal to the layer */
  inline TEveVectorD GetOrtVersor() const {return plane_ortversor;};
  
  /*! Check if a position falls in the layer */
  bool InLayer(const TEveVectorD position);

  /*! Get the sensor type */
  inline std::string GetSensorType() const {return sensor_type;};
  
  /*! Get the hit resolution */
  inline double GetHitResolution() const {return hit_resolution;};

  /*! Get the hit efficiency */
  inline double GetHitEfficiency() const {return hit_efficiency;};

  /*! Get the noise probability */
  inline double GetNoiseProb() const {return noise_prob;};

  /*! Get the x size of the layer */
  inline double GetXSize() const {return x_size;};

  /*! Get the y size of the layer */
  inline double	GetYSize() const {return y_size;};

  /*! Get the x_max of the layer in the detector reference */
  inline double GetXMax() const
      {return center_position[0] + (y_size/2)*cos(dxdy) + (x_size/2)*sin(dxdy);};

  /*! Get the x_min of the layer in the detector reference */
  inline double GetXMin() const	
      {return center_position[0] - (y_size/2)*cos(dxdy) - (x_size/2)*sin(dxdy);};

  /*! Get the y_max of the layer in the detector reference */
  inline double GetYMax() const	
      {return center_position[1] + (y_size/2)*sin(dxdy) + (x_size/2)*cos(dxdy);};

  /*! Get the y_min of the layer in the detector reference */
  inline double GetYMin() const
      {return center_position[1] - (y_size/2)*sin(dxdy) - (x_size/2)*cos(dxdy);};

  /*! Get the z position of the layer */
  inline double GetZ() const {return center_position[2];};
  
  /*! Get the sensor segmentation on x */
  inline double GetXSegm() const {return x_segm;};

  /*! Get the sensor segmentation on y */
  inline double GetYSegm() const {return y_segm;};

  /*! Get the tilt on the xy plane (as an angle) */
  inline double GetDxDy() const {return dxdy;};

  /*! Get the projection matrix */
  inline TMatrixD GetProjectionMatrix() const {return proj_matrix;};

  /*! Get the percentage of energy radiation per layer */
  inline double GetXoverXZero() const {return x_over_Xzero;}; 

  /*! Compute and returns the channelID for a given sensor channel */
  std::string GetChannelID(TFRLayer *layer, const double x, const double y);
  
 protected:
  
 private:

  /*! Kind of sensor */
  std::string sensor_type;
  
  /*! Layer ID */
  unsigned int layer_id;

  /*! Position of the center of the plane [cm] */
  TEveVectorD center_position;

  /*! Layer sizes [cm] */
  double x_size;
  double y_size;
  
  /*! Euler angles, to describe the 3D-rotation of the layer [rad] */
  double phi, theta, psi;

  /*! Rotation matrix in the cartesian coordinate system */
  double rot_matrix[3][3];

  /*! Angular tilt of the sensors on the x-y plane */
  double dxdy;

  /*! Projection matrix translating a state to the measurement coordinate system*/
  TMatrixD proj_matrix;
  
  /*! Versor ortogonal to the layer */
  TEveVectorD plane_ortversor;

  /*! Hit resolution */
  double hit_resolution;

  /*! Hit efficiency */
  double hit_efficiency;

  /*! Noise probability of the sensor */
  double noise_prob;

  /*! Sensor segmentation on x and y coordinates [cm] */
  double x_segm;
  double y_segm;

  /*! Percentage of energy radiation per layer */
  double x_over_Xzero;
  
  ClassDef(TFRLayer, 1)
    
} ;

/*! Operator needed to create maps indexed by layers */
inline bool operator< (const TFRLayer &layer1, const TFRLayer &layer2)
  {return layer1.GetPosition()[2] < layer2.GetPosition()[2];}

/*! Function to compare layers by z position */
inline bool compare_z (TFRLayer *layer1, TFRLayer *layer2)
  {return layer1->GetPosition()[2] < layer2->GetPosition()[2];}

/*! Array of layers */
typedef TObjArray TFRLayers;

#endif // INCLUDE_TFRLAYER_H
