
# source runTest.sh

#set the project directory variable, useful to handle some output/input locations
export PROJECT_DIR=$(pwd)

#compile everything
rm -rf build
mkdir build
cd build
cmake ../
make -j 4
cd ../

#create the output folder
#rm -rf output
mkdir output

#run the test: create a track and propagate it
#./build/bin/software_test -c config/config_test.info -o output/track_test.root

#run a plotting test
#root -l scripts/plot_test.cpp++\(\"$PROJECT_DIR/output/detector_hits.root\",\"track_list\"\)


###########

# tracking detector --> magnetic field --> tracking detector

#run the event generation
./build/bin/event_sim -e 3 -c config/event.info -o output/event_generation.root

#run the detector simulation
./build/bin/detector_sim -i output/event_generation.root -g config/config_geo_allpixel.info -o output/detector_hits_noBinDetector.root

#run the reconstruction
./build/bin/reconstruction -i output/detector_hits_noBinDetector.root -g config/config_geo_allpixel.info -o output/reconstruction_noBinDetector.root

#make the output ntuples
./build/bin/make_ntuples -i output/reconstruction_noBinDetector.root -o output/out_ntuples.root

###########

#to generate Doxygen documentation
#doxygen ./include/Doxyfile.in

#to navigate the documentation
#firefox index.html
